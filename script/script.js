// ПЛАВНЫЙ СКРОЛ ПО ЯКАРЯМ
$(function () {
    $('a[data-target^="anchor"]').bind('click.smoothscroll',function () {
       let target = $(this).attr('href'),
           bl_top = $(target).offset().top;
       $('body, html').animate({scrollTop: bl_top}, 1300);
       return false;
    })
});
// ПЛАВНЫЙ СКРОЛ ПО Конец
/*button UP start*/
const $btnTop = $(".btn-top")
$(window).on("scroll", function () {
    if ($(window).scrollTop() >= 20){
        $btnTop.fadeIn();
    }else{
        $btnTop.fadeOut();
    }
});
$btnTop.on("click",function () {
    $("html,body").animate({scrollTop:0},900)
});
/*button UP end*/

// Скрытие slide-toggle начало
$( document ).ready(function(){
    $( ".slide-toggle" ).click(function(){
        $( ".header" ).slideToggle();
    });
});
// Скрытие slide-toggle конец